import matplotlib.pyplot as plt
import numpy as np
import random
import sys

def experiment(tries,min=0,max=10) -> None:
    numbers = []
    for _ in range (tries) :
        numbers.append(random.randint(min,max))
    print("Generated :" + str(numbers))

    plt.hist(numbers,range=(min,max),bins=max+1,color='blue',edgecolor='black')
    print(f"average : {np.mean(numbers)}")
    plt.show()

if __name__ == '__main__':
    if len(sys.argv) == 2 :
        experiment(int(sys.argv[1]))
    elif len(sys.argv) == 4 :
        experiment(int(sys.argv[1]),int(sys.argv[2]),int(sys.argv[3]))